/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.android.systemui.statusbar.policy;

import android.content.Context;
import android.net.NetworkCapabilities;

import com.android.settingslib.AccessibilityContentDescriptions;
import com.android.settingslib.SignalIcon.IconGroup;
import com.android.settingslib.SignalIcon.State;
import com.android.systemui.statusbar.policy.NetworkController.IconState;
import com.android.systemui.statusbar.policy.NetworkController.SignalCallback;

import java.util.BitSet;
import android.util.Log;

import java.io.FileDescriptor;
import java.io.File;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import android.os.SystemProperties;
import java.net.NetworkInterface;
import java.util.Collections;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public class EthernetSignalController extends
        SignalController<State, IconGroup> {

    public EthernetSignalController(Context context,
            CallbackHandler callbackHandler, NetworkControllerImpl networkController) {
        super("EthernetSignalController", context, NetworkCapabilities.TRANSPORT_ETHERNET,
                callbackHandler, networkController);
        mCurrentState.iconGroup = mLastState.iconGroup = new IconGroup(
                "Ethernet Icons",
                EthernetIcons.ETHERNET_ICONS,
                null,
                AccessibilityContentDescriptions.ETHERNET_CONNECTION_VALUES,
                0, 0, 0, 0,
                AccessibilityContentDescriptions.ETHERNET_CONNECTION_VALUES[0]);
    }

    @Override
    public void updateConnectivity(BitSet connectedTransports, BitSet validatedTransports) {
        mCurrentState.connected = connectedTransports.get(mTransportType);

        if(!mCurrentState.connected){
            mCurrentState.connected = checkEthAux();
        }
        super.updateConnectivity(connectedTransports, validatedTransports);
    }

    @Override
    public void notifyListeners(SignalCallback callback) {
        boolean ethernetVisible = mCurrentState.connected;
        String contentDescription = getTextIfExists(getContentDescription()).toString();
        // TODO: wire up data transfer using WifiSignalPoller.
        callback.setEthernetIndicators(new IconState(ethernetVisible, getCurrentIconId(),
                contentDescription));
    }

    private boolean checkEthAux(){
        String ifaceName = SystemProperties.get("ro.net.eth_aux", "eth1");
        if(getEthernetCarrierState(ifaceName) == 1){
            try {
                List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
                for (NetworkInterface intf : interfaces) {
                    if (ifaceName != null) {
                        if (!intf.getName().equalsIgnoreCase(ifaceName)) continue;
                    }
                    
                    Enumeration<InetAddress> inetAddressesList = intf.getInetAddresses();
                
                    while (inetAddressesList.hasMoreElements()) {
                        InetAddress inetAddress = inetAddressesList.nextElement();

                        if (inetAddress.isSiteLocalAddress() && !inetAddress.isLoopbackAddress() // 127.开头的都是lookback地址
                            && inetAddress.getHostAddress().indexOf(":") == -1) {
                            String address = inetAddress.getHostAddress();
                            return true;
                        }
                    }
                    
                }
            } catch (Exception ex) { 
            } // for now eat exceptions

        }
        return false;
    }

    public int getEthernetCarrierState(String ifname) {
        if(ifname != "") {
            try {
                File file = new File("/sys/class/net/" + ifname + "/carrier");
                String carrier = ReadFromFile(file);
                return Integer.parseInt(carrier);
            } catch(Exception e) {
                e.printStackTrace();
                return 0;
            }
        } else {
            return 0;
        }
    }

    private String ReadFromFile(File file) {
        if((file != null) && file.exists()) {
            try {
                FileInputStream fin= new FileInputStream(file);
                BufferedReader reader= new BufferedReader(new InputStreamReader(fin));
                String flag = reader.readLine();
                fin.close();
                return flag;
            } catch(Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    @Override
    public int getContentDescription() {
        if (mCurrentState.connected) {
            return getIcons().contentDesc[1];
        } else {
            return getIcons().discContentDesc;
        }
    }

    @Override
    public State cleanState() {
        return new State();
    }
}
